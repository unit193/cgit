Source: cgit
Section: net
Priority: optional
Maintainer: Debian cgit Maintainers <team+cgit@tracker.debian.org>
Uploaders:
 Alexander Wirt <formorer@debian.org>,
 Peter Colberg <peter@colberg.org>,
Build-Depends:
 asciidoc-base | asciidoc,
 debhelper-compat (= 13),
 dh-apache2,
 docbook-xsl,
 liblua5.4-dev,
 libssl-dev,
 libxml2-utils,
 lzip,
 pkg-config,
 strace [linux-any],
 tidy,
 unzip,
 xmlto,
 zlib1g-dev,
 zstd,
Standards-Version: 4.6.2
Homepage: https://git.zx2c4.com/cgit/
Vcs-Git: https://salsa.debian.org/debian/cgit.git
Vcs-Browser: https://salsa.debian.org/debian/cgit
Rules-Requires-Root: binary-targets

Package: cgit
Architecture: any
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${misc:Recommends},
Suggests:
 python3,
 python3-docutils,
 python3-markdown,
 python3-pygments | highlight,
Description: hyperfast web frontend for git repositories written in C
 This is an attempt to create a fast web interface for the Git SCM, using a
 built-in cache to decrease server I/O pressure.
 .
 Features:
  * basic repository browsing (logs, diffs, trees...)
  * caching of generated HTML
  * cloneable URLs (implements dumb HTTP transport)
  * commit feeds (atom format)
  * discovery of Git repositories
  * on-the-fly archives for tags and commits
  * plugin support for e.g. syntax highlighting
  * side-by-side diffs
  * simple time/author statistics
  * simple virtual hosting support (macro expansion)
  * understands GitWeb project-lists
  * understands gitweb.owner in Git config files
  * has extensive filtering framework using scripts or a built-in Lua
    interpreter
